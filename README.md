# imagezoomrotatevue

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# Image Sources
Paste, Drag, File Select

# To Do
Vue-ify the drag events  - set lots of `v-on`'s on the drag element?

# To Solve

## How to build Main and Preview from same data
Both Main and Preview are a rendering of the viewport offset from the image's top left.
Main draws image at native size and offsets it by a negative value.
Preview draws image to fit centered in a canvas and draws a rectangle on top.
Zoom/Rotate anchor should be the center of the image

Both can be represented by defining the viewport's dimensions as percentages of the image size.

Known values:
- Image width and height
- Canvas width and height
- Zoom level

if canvas is 640x480
and image is 1000x900
and zoom = 1
and canvas is at top left of image

width = 0.64 = canvasWidth / (imageWidth * zoom) = 640 / (1000 * 1)
height = 0.5333 = canvasHeight / (imageHeight * zoom) = 480 / (900 * 1)
minX = 0.32 = (canvasWidth / 2) / (imageWidth * zoom) = (640 / 2) / (1000 * 1)
minY = 0.2667 = (canvasHeight / 2) / (imageHeight * zoom) = (480 / 2) / (900 * 1)
maxX = 0.68 = 1 - 0.32

x = 0.38
y = 0.39

mainX = 60 = (imageWidth * centerXPercent) - (canvasWidth * zoom / 2) = (1000 * 0.38) - (640 / 2)
mainY = 111 = (imageHeight * centerYPercent) - (canvasHeight * zoom / 2) = (900 * 0.39) - (480 / 2)

imageFitWidth = 533
imageFitHeight = 480

previewW = 341.12 = imageFitWidth * zoom * widthPercentage = 533 * 1 * 0.64
previewH = 255.984 = imageFitHeight * zoom * heightPercentage = 480 * 1 * 0.5333
previewX = 32.04 = (imageFitWidth * zoom * widthPercentage) - (previewW / 2) = (533 * 1 * 0.38) - (341 / 2)
previewY = 59.208 = (imageFitHeight * zoom * heightPercentage) - (previewH / 2) = (480 * 1 * 0.39) - (255.984 / 2)

## Properties to track in Store per frame
- zoom
- rotation
- width (percentage)
- height (percentage)
- x (percentage)
- y (percentage)

## How to handle dragging Main view and Preview box and update the other
Calculate new x/y/w/h to percentages

## Process
When an image is loaded
Render it centered in three frames
With each frame zoomed in further than the last

## Output size
I wanted to be able to specify the width and height of each frame
This is difficult to do nicely when space is limited
Allow arbitrary sizing and scale it to fit the visible area

# To Do
- Moving a panel should move all subsequent panels unless those panels have been moved

# Refactor Structure

whenever any value related to position or size changes
call update function that sorts out positioning, including enforcing boundaries
call render controller function that then calls functions to draw
  affected panel
  affected panel preview
  result preview

should all be completely and entirely separate:
  code for handling user input
  code for handling updates
  code for handling rendering
should all be unit tested
should use requestAnimationFrame to redraw
kill document move listener on drag end, recreate on new drag?

preview panel seems unnecessary, same job as the actual main interface?
could be useful if the main panels go off-screen

# Icon Attribution
<a href="https://thenounproject.com/sommerladboy/collection/mouse/">Mouse Left Click</a> by <a href="https://thenounproject.com/sommerladboy/">Benjamin Sommerlad</a> from the <a href="https://thenounproject.com/">Noun Project</a>

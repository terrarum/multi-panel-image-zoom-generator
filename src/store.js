import Vue from 'vue';
import Vuex from 'vuex';
import Panel from './models/Panel';
import Image from './models/Image';
import constrainImageToCanvas from './helpers/constrainImageToCanvas';
import nearestFive from './helpers/nearestFive';

Vue.use(Vuex);

let panelId = 0;

export default new Vuex.Store({
  state: {
    dirty: false,
    image: null,
    defaultPanelCount: 3,
    canvasWidth: 500,
    canvasHeight: 250,
    panels: [],
  },
  mutations: {
    setImageElement(state, imageElement) {
      state.image = new Image(imageElement);
    },
    addPanel(state, panelOptions) {
      const panel = new Panel(panelId);
      panel.zoom = panelOptions.zoom || 1;
      state.panels.push(panel);
      panelId += 1;
    },
    removePanel(state, panel) {
      let index = 0;

      for (; index < state.panels.length; index += 1) {
        if (panel.id === state.panels[index].id) {
          break;
        }
      }

      state.panels.splice(index, 1);
    },
    reset(state) {
      state.dirty = false;
      state.panels = [];
      state.imageElement = null;
      state.canvasWidth = 500;
      state.canvasHeight = 250;
    },
    setCanvasWidth(state, value) {
      state.canvasWidth = parseInt(value, 10);
    },
    setCanvasHeight(state, value) {
      state.canvasHeight = parseInt(value, 10);
    },
    setPanelCanvas(state, { panel, canvas }) {
      panel.ctx = canvas.getContext('2d');
    },
    setPanelZoom(state, { panel, value }) {
      state.dirty = true;
      panel.zoom = nearestFive(value);
    },
    setPanelMinZoom(state, { panel, value }) {
      panel.minZoom = nearestFive(value);
    },
    setPanelWidth(state, { panel, value }) {
      panel.width = value;
    },
    setPanelHeight(state, { panel, value }) {
      panel.height = value;
    },
    setPanelPosition(state, { panel, position }) {
      state.dirty = true;
      panel.x = position.x;
      panel.y = position.y;
    },
  },
  actions: {
    setImageElement({ commit }, imageElement) {
      commit('setImageElement', imageElement);
    },
    addPanel({ commit }, panelOptions = {}) {
      commit('addPanel', panelOptions);
    },
    removePanel({ commit }, panel) {
      commit('removePanel', panel);
    },
    reset({ commit }) {
      commit('reset');
    },
    setPanelZoom({ commit }, { panel, value: zoom }) {
      const actualWidth = this.state.image.width * zoom;
      const width = this.state.canvasWidth / actualWidth;
      const actualHeight = this.state.image.height * zoom;
      let height = this.state.canvasHeight / actualHeight;
      height = height > 1 ? 1 : height;

      // This part might be unnecessary now.
      if (height > 1 || width > 1) {
        commit('setPanelMinZoom', { panel, value: zoom });
      }

      commit('setPanelWidth', { panel, value: width });
      commit('setPanelHeight', { panel, value: height });
      commit('setPanelZoom', { panel, value: zoom });

      commit('setPanelPosition', {
        panel,
        position: constrainImageToCanvas(panel, panel.x, panel.y),
      });
    },
    setCanvasWidth({ commit, state, getters }, value) {
      commit('setCanvasWidth', value);
      state.panels.forEach((panel) => {
        const heightZoom = getters.canvasHeight / getters.image.height;
        const widthZoom = value / getters.image.width;
        const calculatedZoom = heightZoom >= widthZoom ? heightZoom : widthZoom;

        const zoom = panel.zoom > calculatedZoom ? panel.zoom : calculatedZoom;

        if (panel.zoom <= calculatedZoom) {
          commit('setPanelMinZoom', { panel, value: calculatedZoom });
        }
        else {
          const minHeight = getters.canvasHeight / getters.image.height;
          const minWidth = getters.canvasWidth / getters.image.width;
          const lowestZoom = minHeight >= minWidth ? minHeight : minWidth;
          commit('setPanelMinZoom', { panel, value: lowestZoom });
        }

        this.dispatch('setPanelZoom', {
          panel,
          value: zoom,
        });
      });
    },
    setCanvasHeight({ commit, state, getters }, value) {
      commit('setCanvasHeight', value);
      state.panels.forEach((panel) => {
        const heightZoom = value / getters.image.height;
        const widthZoom = getters.canvasWidth / getters.image.width;
        const calculatedZoom = heightZoom >= widthZoom ? heightZoom : widthZoom;

        const zoom = panel.zoom > calculatedZoom ? panel.zoom : calculatedZoom;

        // When the canvas size changes, recalculate the minimum zoom.
        if (panel.zoom <= calculatedZoom) {
          commit('setPanelMinZoom', { panel, value: calculatedZoom });
        }
        else {
          const minHeight = getters.canvasHeight / getters.image.height;
          const minWidth = getters.canvasWidth / getters.image.width;
          const lowestZoom = minHeight >= minWidth ? minHeight : minWidth;
          commit('setPanelMinZoom', { panel, value: lowestZoom });
        }

        this.dispatch('setPanelZoom', {
          panel,
          value: zoom,
        });
      });
    },
  },
  getters: {
    devicePixelRatio: state => state.devicePixelRatio,
    canvasWidth: state => state.canvasWidth,
    canvasHeight: state => state.canvasHeight,
    image: state => state.image,
    defaultPanelCount: state => state.defaultPanelCount,
    panelCount: state => state.panels.length,
    panels: state => state.panels,
    isDirty: state => state.dirty,
  },
});

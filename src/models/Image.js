export default imageElement => ({
  imageElement,
  width: imageElement.width,
  height: imageElement.height,
});

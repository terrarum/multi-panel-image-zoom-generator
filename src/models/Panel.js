export default panelId => ({
  id: panelId,
  zoom: 1,
  minZoom: 0.05,
  x: 0.5,
  y: 0.5,
  width: 0,
  height: 0,
  ctx: null,
});

import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import store from './store';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/edit',
      name: 'edit',
      beforeEnter: (to, from, next) => {
        if (store.getters.image === null) {
          next({ name: 'home' });
        }
        else {
          next();
        }
      },
      component: () => import(/* webpackChunkName: "imageeditor" */ './views/ImageEditor.vue'),
    },
  ],
});

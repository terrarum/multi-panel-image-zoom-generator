import imageController from './image';
import store from '@/store';

const processImage = async function processImage(event) {
  const image = await imageController.convertToImageElement(event, 'select');
  store.dispatch('setImageElement', image);
};

const selectImage = function selectImage() {
  const fileInput = document.createElement('input');
  fileInput.addEventListener('change', processImage, false);
  fileInput.type = 'file';
  fileInput.click();
};

export default {
  selectImage,
};

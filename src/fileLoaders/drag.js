import imageController from './image';
import store from '@/store';

const dragenter = function dragenter(event) {
  if (event.target.classList) {
    event.target.classList.add('is-drag');
  }
};

const dragleave = function dragleave(event) {
  if (event.target.classList) {
    event.target.classList.remove('is-drag');
  }
};

const dragover = function dragover(event) {
  event.preventDefault();
  event.stopPropagation();
};

const drop = async function drop(event) {
  event.preventDefault();
  event.stopPropagation();
  const image = await imageController.convertToImageElement(event, 'drag');
  store.dispatch('setImageElement', image);
};

const init = function init(dropTarget) {
  dropTarget.addEventListener('dragenter', dragenter);
  dropTarget.addEventListener('dragleave', dragleave);
  dropTarget.addEventListener('dragover', dragover);
  dropTarget.addEventListener('drop', drop);
};

const destroy = function destroy(dropTarget) {
  if (dropTarget) {
    dropTarget.removeEventListener('dragenter', dragenter);
    dropTarget.removeEventListener('dragleave', dragleave);
    dropTarget.removeEventListener('dragover', dragover);
    dropTarget.removeEventListener('drop', drop);
  }
};

export default {
  init,
  destroy,
};

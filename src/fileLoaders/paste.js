import imageController from './image';
import store from '@/store';

const pasteListener = async function pasteListener(event) {
  const image = await imageController.convertToImageElement(event, 'paste');
  const devicePixelRatio = window.devicePixelRatio || 1;

  image.width /= devicePixelRatio;
  image.height /= devicePixelRatio;

  store.dispatch('setImageElement', image);
};

const init = function init() {
  window.addEventListener('paste', pasteListener);
};

const destroy = function destroy() {
  window.removeEventListener('paste', pasteListener);
};

export default {
  init,
  destroy,
};

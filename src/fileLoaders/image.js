const base64ToImageElement = function base64ToImageElement(imageData) {
  return new Promise((resolve) => {
    const image = new Image();
    image.onload = () => {
      resolve(image);
    };
    image.src = imageData;
  });
};

const fileToBase64 = function fileToBase64(file) {
  return new Promise((resolve) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      resolve(event.target.result);
    };
    reader.readAsDataURL(file);
  });
};

const getImageFile = async function getImageFile(items) {
  let blob = null;
  for (let i = 0, l = items.length; i < l; i += 1) {
    const item = items[i];
    if (item.kind === 'file' && item.type.startsWith('image')) {
      blob = item;
    }
  }

  if (blob !== null) {
    return blob.getAsFile();
  }
  return null;
};

const convertToImageElement = async function convertToImageElement(source, type) {
  if (type === 'base64') {
    return base64ToImageElement(source);
  }

  let file = null;
  if (type === 'paste') {
    file = await getImageFile(source.clipboardData.items);
  }
  else if (type === 'drag') {
    file = await getImageFile(source.dataTransfer.items);
  }
  else if (type === 'select') {
    [file] = source.target.files;
  }
  const base64 = await fileToBase64(file);

  return base64ToImageElement(base64);
};

export default {
  convertToImageElement,
};

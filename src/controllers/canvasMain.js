import store from '../store';
import fitRectangle from '../helpers/fitRectangle';
import constrainImageToCanvas from '../helpers/constrainImageToCanvas';

const wait = 16; // Debounce ms, 16 = 60fps. Should really use raf.
let last = 0;

const updateCanvas = function updateCanvas(canvas, canvasContainer) {
  const {
    canvasWidth,
    canvasHeight,
  } = store.getters;

  const dimensions = fitRectangle(
    canvasContainer.clientWidth,
    canvasContainer.clientHeight,
    canvasWidth,
    canvasHeight,
  );

  canvas.style.width = `${dimensions.width}px`;
  canvas.style.height = `${dimensions.height}px`;
  canvas.width = dimensions.width;
  canvas.height = dimensions.height;
};

const render = function render(canvas, canvasContainer, panel, image) {
  const ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

  const {
    canvasWidth,
    canvasHeight,
  } = store.getters;

  const dimensions = fitRectangle(
    canvasContainer.clientWidth,
    canvasContainer.clientHeight,
    canvasWidth,
    canvasHeight,
  );

  const imageWidth = image.width * panel.zoom;
  const imageHeight = image.height * panel.zoom;

  const x = -(imageWidth * panel.x) + canvasWidth / 2;
  const y = -(imageHeight * panel.y) + canvasHeight / 2;

  ctx.drawImage(
    image.imageElement,
    x * dimensions.scaleRatio,
    y * dimensions.scaleRatio,
    imageWidth * dimensions.scaleRatio,
    imageHeight * dimensions.scaleRatio,
  );
};

const movementListeners = function movementListeners(canvas, panel, image) {
  let drag = false;

  let startX = 0;
  let startY = 0;
  let initialX = null;
  let initialY = null;

  canvas.addEventListener('mousedown', (event) => {
    last = Date.now();
    startX = event.offsetX;
    startY = event.offsetY;

    initialX = Object.assign({}, {
      x: panel.x,
    }).x;
    initialY = Object.assign({}, {
      y: panel.y,
    }).y;

    drag = true;
  });

  document.addEventListener('mouseup', () => {
    if (drag) {
      drag = false;
      document.body.classList.remove('no-select');
    }
  });

  // Add the listener to document instead of the canvas so that the user
  // can keep moving the image if their cursor leaves the canvas.
  document.addEventListener('mousemove', (event) => {
    if (drag && Date.now() - last > wait) {
      last = Date.now();
      document.body.classList.add('no-select');

      // Get the mouse position relative to the canvas.
      const mouseX = event.pageX - canvas.offsetLeft;
      const mouseY = event.pageY - canvas.offsetTop;

      // The distance from where the user started dragging
      // to where the mouse is now.
      const deltaX = mouseX - startX;
      const deltaY = mouseY - startY;

      // Adjust the distance by the zoom level.
      const dX = deltaX / (image.width * panel.zoom);
      const dY = deltaY / (image.height * panel.zoom);

      // Set final x/y coordinates.
      const x = initialX - dX;
      const y = initialY - dY;

      const position = constrainImageToCanvas(panel, x, y);

      store.commit('setPanelPosition', {
        panel,
        position,
      });
    }
  });
};

/**
 * Calculate the minimum safe zoom.
 *
 * @param imageWidth
 * @param imageHeight
 * @param canvasWidth
 * @param canvasHeight
 * @return {number}
 */
const calculateMinZoom = function calculateMinZoom(
  imageWidth,
  imageHeight,
  canvasWidth,
  canvasHeight,
) {
  const minZoomWidth = canvasWidth / imageWidth;
  const minZoomHeight = canvasHeight / imageHeight;

  return minZoomWidth >= minZoomHeight ? minZoomWidth : minZoomHeight;
};

const init = function init(panel, refs) {
  const {
    image,
    canvasWidth,
    canvasHeight,
  } = store.getters;

  const { canvasMain: canvas, canvasMainContainer: canvasContainer } = refs;

  const actualWidth = image.width * panel.zoom;
  const width = canvasWidth / actualWidth;
  const actualHeight = image.height * panel.zoom;
  const height = canvasHeight / actualHeight;

  store.commit('setPanelWidth', { panel, value: width });
  store.commit('setPanelHeight', { panel, value: height });

  // Calculate lowest zoom level.
  const minZoom = calculateMinZoom(image.width, image.height, canvasWidth, canvasHeight);
  store.commit('setPanelMinZoom', {
    panel,
    value: minZoom,
  });
  // Set initial zoom level if image is smaller than canvas.
  if (panel.width > 1 || panel.height > 1) {
    store.dispatch('setPanelZoom', {
      panel,
      value: minZoom,
    });
  }

  movementListeners(canvas, panel, image);
  updateCanvas(canvas, canvasContainer);
  render(canvas, canvasContainer, panel, image);

  store.commit('setPanelCanvas', { panel, canvas });

  const mutations = [
    'setPanelWidth',
    'setPanelHeight',
    'setPanelPosition',
  ];

  store.subscribe(({ type, payload }) => {
    if (type === 'setCanvasWidth') {
      updateCanvas(canvas, canvasContainer);
      render(canvas, canvasContainer, panel, image);
    }

    if (type === 'setCanvasHeight') {
      updateCanvas(canvas, canvasContainer);
      render(canvas, canvasContainer, panel, image);
    }

    if (mutations.includes(type) && payload.panel.id === panel.id) {
      render(canvas, canvasContainer, panel, image);
    }
  });
};

export default {
  init,
  constrainImageToCanvas,
};

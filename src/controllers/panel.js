import canvasMainController from './canvasMain';
import canvasOverviewController from './canvasOverview';

const init = function init(panel, refs) {
  canvasMainController.init(panel, refs);
  canvasOverviewController.init(panel, refs);
};

export default {
  init,
};

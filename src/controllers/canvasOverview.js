import store from '../store';
import fitRectangle from '../helpers/fitRectangle';

const renderSelection = function renderSelection(canvas, panel) {
  const ctx = canvas.getContext('2d');

  const width = canvas.width * panel.width;
  const height = canvas.height * panel.height;
  const x = canvas.width * panel.x - width / 2;
  const y = canvas.height * panel.y - height / 2;

  ctx.strokeRect(x, y, width, height);
};

const renderImage = function render(canvas, canvasContainer, panel, image) {
  const ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  const containerWidth = canvasContainer.clientWidth;
  const containerHeight = canvasContainer.clientHeight;
  const imageWidth = image.width;
  const imageHeight = image.height;

  const dimensions = fitRectangle(containerWidth, containerHeight, imageWidth, imageHeight);

  canvas.style.width = `${dimensions.width}px`;
  canvas.width = dimensions.width;
  canvas.style.height = `${dimensions.height}px`;
  canvas.height = dimensions.height;

  const x = 0;
  const y = 0;
  ctx.drawImage(store.getters.image.imageElement, x, y, dimensions.width, dimensions.height);
};

const init = function init(panel, refs) {
  const { canvasOverview, canvasContainer } = refs;

  const {
    image,
  } = store.getters;

  renderImage(canvasOverview, canvasContainer, panel, image);
  renderSelection(canvasOverview, panel);

  const mutations = [
    'setPanelWidth',
    'setPanelHeight',
    'setPanelPosition',
  ];

  store.subscribe(({ type, payload }) => {
    if (type === 'setCanvasWidth' || type === 'setCanvasHeight') {
      renderImage(canvasOverview, canvasContainer, panel, image);
      renderSelection(canvasOverview, panel);
    }

    if (mutations.includes(type) && payload.panel.id === panel.id) {
      renderImage(canvasOverview, canvasContainer, panel, image);
      renderSelection(canvasOverview, panel);
    }
  });
};

export default {
  init,
};

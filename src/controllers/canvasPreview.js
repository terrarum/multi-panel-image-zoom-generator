import store from '../store';
import fitRectangle from '../helpers/fitRectangle';

const setCanvasDimensions = function setCanvasDimensions(ctx, canvasDimensions) {
  ctx.canvas.width = canvasDimensions.width;
  ctx.canvas.height = canvasDimensions.height;
};

/**
 * Duplicated in download.js
 *
 * @param ctx
 * @param panels
 * @param canvasDimensions
 */
const renderResult = function renderResult(ctx, panels, canvasDimensions) {
  let i = 0;

  // Render panels.
  panels.forEach((panel) => {
    if (panel.ctx === null) {
      return;
    }

    const panelWidth = canvasDimensions.width;
    const panelHeight = canvasDimensions.height / panels.length;
    const x = 0;
    const y = panelHeight * i;

    ctx.drawImage(panel.ctx.canvas, x, y, panelWidth, panelHeight);

    i += 1;
  });

  // Render dividing lines.
  for (let panelIndex = 1; panelIndex < panels.length; panelIndex += 1) {
    const { width } = canvasDimensions;
    const y = canvasDimensions.height / panels.length * panelIndex + 0.5;
    ctx.beginPath();
    ctx.moveTo(0, y);
    ctx.lineTo(width, y);
    ctx.stroke();
  }
};

const update = function update(previewContainer, previewCanvas) {
  // Get dependencies.
  const ctx = previewCanvas.getContext('2d');
  const { panels, canvasWidth: panelWidth, canvasHeight: panelHeight } = store.getters;

  const sidebarWidth = previewContainer.clientWidth;
  const sidebarHeight = previewContainer.clientHeight;
  const canvasWidth = panelWidth;
  const canvasHeight = panelHeight * panels.length;

  const canvasSize = fitRectangle(sidebarWidth, sidebarHeight, canvasWidth, canvasHeight);

  // Set dimensions.
  setCanvasDimensions(ctx, canvasSize);

  // Render result.
  renderResult(ctx, panels, canvasSize);
};

const init = function init(previewContainer, previewCanvas) {
  const mutations = [
    'setPanelWidth',
    'setPanelHeight',
    'setPanelPosition',
    'setPanelCanvas',
    'addPanel',
    'removePanel',
  ];

  store.subscribe(({ type }) => {
    if (mutations.includes(type)) {
      update(previewContainer, previewCanvas);
    }
  });
};

export default {
  init,
  update,
};

import store from '../store';

function createCanvas(width, height) {
  const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  return canvas;
}

/**
 * Duplicated in canvasPreview.js
 *
 * @param panels
 * @param canvas
 */
function renderToCanvas(panels, canvas) {
  const ctx = canvas.getContext('2d');

  let i = 0;
  panels.forEach((panel) => {
    if (panel.ctx === null) {
      return;
    }

    const panelWidth = canvas.width;
    const panelHeight = canvas.height / panels.length;
    const x = 0;
    const y = panelHeight * i;

    ctx.drawImage(panel.ctx.canvas, x, y, panelWidth, panelHeight);

    i += 1;
  });

  // Render dividing lines.
  for (let panelIndex = 1; panelIndex < panels.length; panelIndex += 1) {
    const { width } = canvas;
    const y = canvas.height / panels.length * panelIndex + 0.5;
    ctx.beginPath();
    ctx.moveTo(0, y);
    ctx.lineTo(width, y);
    ctx.stroke();
  }
}

function triggerDownload(canvas, name) {
  const fakeLink = document.createElement('a');
  fakeLink.download = name;
  fakeLink.href = canvas.toDataURL();

  if (document.createEvent) {
    const e = document.createEvent('MouseEvents');
    e.initMouseEvent('click', true, true, window,
      0, 0, 0, 0, 0, false, false, false,
      false, 0, null);

    fakeLink.dispatchEvent(e);
  }
  else if (fakeLink.fireEvent !== undefined) {
    fakeLink.fireEvent('onclick');
  }
}

function download() {
  const outputWidth = store.getters.canvasWidth;
  const outputHeight = store.getters.canvasHeight * store.getters.panelCount;

  const outputCanvas = createCanvas(outputWidth, outputHeight);

  const { panels } = store.getters;
  renderToCanvas(panels, outputCanvas);

  triggerDownload(outputCanvas, 'imagemash.png');
}

export default {
  download,
};

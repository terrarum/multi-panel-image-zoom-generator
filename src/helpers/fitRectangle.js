function calculateScaleRatio(outerWidth, outerHeight, innerWidth, innerHeight) {
  const outerRatio = outerWidth / outerHeight;
  const innerRatio = innerWidth / innerHeight;
  let scaleRatio = 1;

  if (outerRatio <= innerRatio) {
    scaleRatio = outerWidth / innerWidth;
  }
  else if (outerRatio > innerRatio) {
    scaleRatio = outerHeight / innerHeight;
  }

  return scaleRatio;
}

module.exports = function fitRectangle(outerWidth, outerHeight, innerWidth, innerHeight) {
  let width = null;
  let height = null;

  const scaleRatio = calculateScaleRatio(
    outerWidth,
    outerHeight,
    innerWidth,
    innerHeight,
  );

  const outerRatio = outerWidth / outerHeight;
  const innerRatio = innerWidth / innerHeight;

  if (innerWidth >= innerHeight && outerRatio < innerRatio) {
    width = outerWidth;
    height = innerHeight * scaleRatio;
  }
  else if (innerWidth > innerHeight && outerRatio > innerRatio) {
    width = innerWidth * scaleRatio;
    height = outerHeight;
  }
  else if (innerWidth <= innerHeight && outerRatio > innerRatio) {
    width = innerWidth * scaleRatio;
    height = outerHeight;
  }
  else if (innerWidth < innerHeight && outerRatio < innerRatio) {
    width = outerWidth;
    height = innerHeight * scaleRatio;
  }
  else if (outerRatio === innerRatio) {
    width = outerWidth;
    height = outerHeight;
  }

  width = Math.round(width);
  height = Math.round(height);

  return {
    width,
    height,
    scaleRatio,
  };
};

module.exports = function constrainImageToCanvas(panel, x, y) {
  const leftBound = panel.width / 2;
  const rightBound = 1 - panel.width + leftBound;
  x = x <= leftBound ? leftBound : x;
  x = x >= rightBound ? rightBound : x;

  const topBound = panel.height / 2;
  const bottomBound = 1 - panel.height + topBound;
  y = y <= topBound ? topBound : y;
  y = y >= bottomBound ? bottomBound : y;

  return {
    x,
    y,
  };
};

module.exports = function nearestFive(val) {
  val = parseFloat(val);
  val *= 100;
  val = parseFloat(val.toFixed(0));
  const sub = 5 - (val % 5);
  return sub === 5 ? val / 100 : (val + sub) / 100;
};

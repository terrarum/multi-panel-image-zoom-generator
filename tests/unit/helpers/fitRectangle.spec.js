import fitRectangle from '../../../src/helpers/fitRectangle';

const sizes = [
  [500, 500, 1024, 1024, 500, 500],
  [500, 500, 1024, 768, 500, 375],
  [500, 500, 768, 1024, 375, 500],
  [100, 500, 1024, 768, 100, 75],
  [100, 500, 768, 1024, 100, 133],
  [100, 500, 1024, 1024, 100, 100],
  [500, 100, 1024, 768, 133, 100],
  [500, 100, 768, 1024, 75, 100],
  [500, 100, 1024, 1024, 100, 100],
  [500, 100, 20, 20, 100, 100],
  [500, 100, 20, 25, 80, 100],
];

sizes.forEach((size) => {
  const outerWidth = size[0];
  const outerHeight = size[1];
  const innerWidth = size[2];
  const innerHeight = size[3];
  const expectedWidth = size[4];
  const expectedHeight = size[5];

  test(`${outerWidth}, ${outerHeight}, ${innerWidth}, ${innerHeight}`, () => {
    const actual = fitRectangle(outerWidth, outerHeight, innerWidth, innerHeight);

    expect(actual.width).toEqual(expectedWidth);
    expect(actual.height).toEqual(expectedHeight);
  });
});
